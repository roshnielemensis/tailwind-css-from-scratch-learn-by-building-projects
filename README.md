# Tailwind CSS From Scratch  Learn By Building Projects

Tailwind css

## Utility-First Fundamentals

```htmlcode
<div class="flex items-center p-6 max-w-sm mx-auto bg-white rounded-xl shadow-lg space-x-4 mt-12">
    <img class="w-12 h-12" src="../assets/img/warning.svg" alt="alert">
    <div>
         <div class="text-xl font-medium text-black">Are You Sure?</div>
        <p class="text-slate-500">You are about to delete a post</p>
    </div>
   </div>
```

### In the example above, we’ve used

```explaination
 => Tailwind’s flexbox and padding utilities (flex, shrink-0, and p-6) to control the overall card layout
 =>   The max-width and margin utilities (max-w-sm and mx-auto) to constrain the card width and center it horizontally
 =>   The background color, border radius, and box-shadow utilities (bg-white, rounded-xl, and shadow-lg) to style the card’s     appearance
 =>   The width and height utilities (w-12 and h-12) to size the logo image
 =>   The space-between utilities (space-x-4) to handle the spacing between the logo and the text
 =>   The font size, text color, and font-weight utilities (text-xl, text-black, font-medium, etc.) to style the card text
```